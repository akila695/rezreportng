package com.rezgateway.automation.reports;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.rezgateway.automation.utill.DriverFactory;

public class ExtentTestNGReportBuilder {

	private static ExtentReports extent;
	private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();

	private static String screenshotPath = "Reports/Screenshots";
	public String TEST_URL = "";

	@BeforeSuite
	public synchronized void beforeSuite() {
		extent = ExtentManager.createInstance("ExtentReport.html");
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("ExtentReport.html");
		extent.attachReporter(htmlReporter);
	}

	@BeforeClass
	public synchronized void beforeClass() {
		ExtentTest parent = extent.createTest(getClass().getName().substring(getClass().getName().lastIndexOf(".") + 1));
		parentTest.set(parent);
	}

	@AfterMethod
	public synchronized void afterMethod(ITestResult result) {

		// System.out.println(result.getStatus());
		ExtentTest child;
		String TestName;
		String Expected;
		String Actual;

		try {
			TestName = result.getAttribute("TestName").toString();
		} catch (Exception e) {
			TestName = "Test Name Unspecified";
		}
		try {
			Expected = result.getAttribute("Expected").toString();
		} catch (Exception e) {
			Expected = "Expected Result Unspecified";
		}
		try {
			Actual = result.getAttribute("Actual").toString();
		} catch (Exception e) {
			Actual = "Actual Result Unspecified";
		}

		child = ((ExtentTest) parentTest.get()).createNode(TestName);
		child.info("Expected :" + Expected);
		child.info("Actual   :" + Actual);

		test.set(child);
		if (result.getStatus() == ITestResult.FAILURE) {
			try {
				System.out.println(result.getAttribute("TestName").toString());
				child.addScreenCaptureFromPath(DriverFactory.getInstance().getScreenShotCreator().getScreenShotPathOnFailure(result.getAttribute("TestName").toString()));
			} catch (Exception e) {
				child.fatal(" Can't get Screenshot due to " + e.toString());
				e.printStackTrace();
			}
			((ExtentTest) test.get()).fail(result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			((ExtentTest) test.get()).skip(result.getThrowable());
		} else {
			((ExtentTest) test.get()).pass("Test passed");
		}
		extent.flush();
	}

	public String takeScreenshot(String testDescription) {

		String FileName = testDescription + ".jpg";

		String path = screenshotPath + "/" + FileName;
		System.out.println(path);
		try {
			System.out.println(DriverFactory.getInstance().getBrowser());
			File scrFile = ((TakesScreenshot) DriverFactory.getInstance().getDriver()).getScreenshotAs(OutputType.FILE);
			System.out.println(scrFile.exists());
			FileUtils.copyFile(scrFile, new File(path));
		} catch (Exception e) {
			e.printStackTrace();
		}
			

		return path;
	}

}
